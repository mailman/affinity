Mailman Affinity
=============

Affinity is an alternative Administrative Interface for Mailman 3 Core. It is looking to incorporate the core features of Mailman 3 with modern interface design. My goal is to use Affinity to particularly empower the roles of Server Owner and List Owner.

There will be one interface to manage users, domains, and mailing lists instead of the current Postorius/Django app.

### New Features to be added:
* Ability to schedule a post sent to a list for list owners.
* Bounce Settings (completed!)
* Pre-defined headers for list owners to use.
* Display of storage and bandwidth usage on per list basis.
* Each list will display how many members it has to both Server Owner and List Owners.
* Display the amount of bounces a list is generating on a daily, weekly, and/or monthly to both Server Owner and List Owners.